<?php
if(isset($_GET['calculate'])){
    if(isset($_GET['numberOne']) && isset($_GET['numberTwo'])){    
        $numberOne = (float) $_GET['numberOne'];
        $numberTwo = (float) $_GET['numberTwo'];
        if($_GET['calcType'] == 'add'){
            $result = $numberOne + $numberTwo;
            $sign = ' + ';
        }elseif($_GET['calcType'] == 'subtract'){
            $result = $numberOne - $numberTwo;
            $sign = ' - ';
        }elseif($_GET['calcType'] == 'multiply'){
            $result = $numberOne * $numberTwo;
            $sign = ' * ';
        }elseif($_GET['calcType'] == 'divide'){
            if($numberTwo!=0){
                $result = $numberOne / $numberTwo;
                $sign = ' / ';
            }else{
                $result = 'Fatal Error: Division by 0';
                $sign = ' / ';
            }
        }
        
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="master.css" rel="stylesheet" type="text/css" />
    <title>Calculator</title>
</head>

<body>

<div class="wrapperCenter">
    <div class="calcOutline">
        <div class="wrapperCenter">
            <form method="get" name="calculator">
                <table class="calculator">
                    <tr class="calcView">
                    <td colspan=4>
                    <?php
                    if(isset($result)){
                        htmlentities(printf("<p class = 'calcView'>%.2f %s %.2f = %s</p>",
                        $numberOne,
                        $sign,
                        $numberTwo,
                        $result));
                    }
                    ?>
                    </td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="reset" class="button" name="clear" value="Clear" /></td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="text" name="numberOne" class="tField" /></td>    
                    </tr>
                    <tr class="calcType">
                        <td>
                            <label>+ <br>(Plus)</label><br> <input type="radio" name="calcType" value="add" class="calcType" checked/>
                        </td>
                        <td>
                            <label>- <br>(Minus)</label><br><input type="radio" name="calcType" value="subtract" class="calcType" />
                        </td>
                        <td>
                            <label>* <br>(Times) </label><br><input type="radio" name="calcType" value = "multiply" class="calcType" />
                        </td>
                        <td>
                            <label>/ <br>(Divide) </label><br><input type="radio" name="calcType" value = "divide" class="calcType" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="text" name="numberTwo" class="tField" /></td>
                    </tr>
                    <tr>
                        <td colspan=4><input type="submit" class="button" name="calculate" value ="Calculate!" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>



</body>
</html>
